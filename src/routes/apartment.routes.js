const express = require('express')
const router = express.Router()
const ApartmentController = require('../controllers/apartment.controller')
const AuthController = require('../controllers/authentication.controller')

// Lijst van movies
router.post('/', AuthController.validateToken, ApartmentController.createApartment)
router.get('/', ApartmentController.getAllApartments)
router.get('/:apartmentId', ApartmentController.getApartmentById)
router.delete('/:apartmentId', AuthController.validateToken, ApartmentController.deleteApartmentById)
router.put('/:apartmentId', AuthController.validateToken, ApartmentController.updateApartment)

router.post('/:apartmentId/reservations', AuthController.validateToken, ApartmentController.createReservation)
router.get('/:apartmentId/reservations', ApartmentController.getAllReservations)
router.get('/:apartmentId/reservations/:reservationId', ApartmentController.getSpecificReservations)
router.delete('/:apartmentId/reservations/:reservationId', AuthController.validateToken, ApartmentController.deleteReservationById)
router.put('/:apartmentId/reservations/:reservationId', AuthController.validateToken, ApartmentController.updateReservation)

module.exports = router
