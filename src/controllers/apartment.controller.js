const logger = require('../config/appconfig').logger
const database = require('../datalayer/mssql.dao')
const assert = require('assert')

const postalCodeValidator = new RegExp('^[0-9]{4}[a-zA-Z]{2}$')

module.exports = {
  createApartment: function(req, res, next) {
    logger.info('Post /api/apartments aangeroepen')

    // Check dat we de UserId in het request hebben - vanuit de validateToken functie.
    logger.trace('User id:', req.userId)

    // hier komt in het request een apartment binnen.
    const apartment = req.body
    logger.info(apartment)
    try {
      assert.equal(typeof apartment.description, 'string', 'apartment.description is required.')
      assert.equal(typeof apartment.streetaddress, 'string', 'apartment.streetaddress is required.')
      assert(postalCodeValidator.test(apartment.postalcode), 'A valid postalcode is required.')
      assert.equal(typeof apartment.city, 'string', 'apartment.city is required.')
    } catch (e) {
      const errorObject = {
        message: e.toString(),
        code: 400
      }
      return next(errorObject)
    }

    const query =
      "INSERT INTO Apartment(Description, StreetAddress, PostalCode, City, UserId)  VALUES ('" +
      apartment.description +
      "','" +
      apartment.streetaddress +
      "','" +
      apartment.postalcode +
      "','" +
      apartment.city +
      "','" +
      req.userId +
      "'); SELECT * FROM Apartment WHERE ApartmentId = SCOPE_IDENTITY();"

    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(201).json({ result: rows.recordset })
      }
    })
  },

  updateApartment: function(req, res, next) {
    logger.info('Put /api/apartments aangeroepen')

    // Check dat we de UserId in het request hebben - vanuit de validateToken functie.
    logger.trace('User id:', req.userId)

    const id = req.params.apartmentId
    const apartment = req.body
    logger.info(apartment)
    try {
      assert(postalCodeValidator.test(apartment.postalcode), 'A valid postalcode is required.')
      assert.equal(typeof apartment.description, 'string', 'apartment.description is required.')
      assert.equal(typeof apartment.streetaddress, 'string', 'apartment.streetaddress is required.')
      assert.equal(typeof apartment.city, 'string', 'apartment.city is required.')
    } catch (e) {
      const errorObject = {
        message: e.toString(),
        code: 400
      }
      return next(errorObject)
    }

    const query =
      "UPDATE Apartment SET Description = '"+
      apartment.description+
      "', StreetAddress = '"+
      apartment.streetaddress +
      "', PostalCode = '" +
      apartment.postalcode +
      "', City = '" +
      apartment.city +
      "', UserId = '" +
      req.userId + "'" +
      `WHERE ApartmentId = ${id}`

    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: apartment })
      }
    })
  },

  getAllApartments: (req, res, next) => {
    logger.info('Get /api/apartments aangeroepen')

    const query =
      'select a.*, u.firstname, u.lastname, r.startdate, r.enddate, r.status ' +
      'from apartment as a' +
      '	join dbuser as u on u.userid = a.userid' +
      '		join reservation as r on r.apartmentid = a.apartmentid'
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
  },

  getApartmentById: function(req, res, next) {
    logger.info('Get /api/apartment/id aangeroepen')
    const id = req.params.apartmentId

    const query = 
    'select a.*, u.firstname, u.lastname, r.startdate, r.enddate, r.status' +
   ' from apartment as a' +
     ' join dbuser as u on u.userid = a.userid' +
     ' join reservation as r on r.apartmentid = a.apartmentid' +
     ' group by a.ApartmentId, a.City, a.Description, a.PostalCode, a.StreetAddress, a.UserId,  u.firstname, u.lastname, r.startdate, r.enddate, r.status' +
     `  HAVING a.ApartmentId = ${id};`
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
  },

  deleteApartmentById: function(req, res, next) {
    logger.info('deleteApartmentById aangeroepen')
    const id = req.params.apartmentId
    const userId = req.userId

    const query = `DELETE FROM Apartment WHERE ApartmentId=${id} AND UserId=${userId}`
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        logger.trace('Could not delete apartment: ', err)
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        if (rows.rowsAffected[0] === 0) {
          // query ging goed, maar geen apartment met de gegeven ApartmentId EN userId.
          // -> retourneer een error!
          const msg = 'Apartment not found or you have no access to this apartment!'
          logger.trace(msg)
          const errorObject = {
            message: msg,
            code: 401
          }
          next(errorObject)
        } else {
          res.status(200).json({ result: 'Succes!' })
        }
      }
    })
  },

  createReservation: function(req, res, next) {
    logger.info('createReservation aangeroepen')

    // Check dat we de UserId in het request hebben - vanuit de validateToken functie.
    logger.trace('User id:', req.userId)

    const apartment = req.body
    const apartmentId = req.params.apartmentId
    const userId = req.userId

    logger.info(`apartmentId = ${apartmentId}, userId = ${userId}`)
    logger.info(apartment)

    try {
      assert.equal(typeof apartment.startdate, 'string', 'apartment.startdate is required.')
      assert.equal(typeof apartment.enddate, 'string', 'apartment.enddate is required.')
      assert.equal(typeof apartment.status, 'string', 'apartment.status is required.')
    } catch (e) {
      const errorObject = {
        message: e.toString(),
        code: 400
      }
      return next(errorObject)
    }

    const query = "INSERT INTO Reservation (ApartmentId, StartDate, EndDate, Status, UserId) VALUES "+
    "('" + apartmentId + "','" + apartment.startdate + "','" + apartment.enddate + "','" + apartment.status + "','" + userId + "')"+
    `SELECT * FROM RESERVATION WHERE UserId = ${userId} AND ApartmentId = ${apartmentId}` 

    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(201).json({ result: rows.recordset })
      }
    })
  },
  getAllReservations: (req, res, next) => {
    logger.info('Get /api/apartments/id/reservations aangeroepen')

    const apartmentId = req.params.apartmentId
    logger.info(apartmentId)
  
    const query =
      'SELECT * ' +
      'FROM Reservation ' +
      "WHERE ApartmentId = '" + 
      apartmentId +
      "'"
      logger.debug(query)
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
  },
  getSpecificReservations: (req, res, next) => {
    logger.info('Get /api/apartments/id/reservations aangeroepen')

    const apartmentId = req.params.apartmentId
    const reservationId = req.params.reservationId

    const query =
      'SELECT * ' +
      'FROM Reservation ' +
      "WHERE ApartmentId = '" + apartmentId + "' AND ReservationId = '" + reservationId + "';"
      logger.debug(query)
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
  },

  updateReservation: function(req, res, next) {
    logger.info('Put /api/apartments/id/reservations aangeroepen')

    // Check dat we de UserId in het request hebben - vanuit de validateToken functie.
    logger.trace('User id:', req.userId)



    const apartmentId = req.params.apartmentId
    const reservationId = req.params.reservationId
    const reservation = req.body
    logger.info(reservation)
    try {
      assert.equal(typeof reservation.status, 'string', 'reservation.status is required.')
    } catch (e) {
      const errorObject = {
        message: e.toString(),
        code: 400
      }
      return next(errorObject)
    }

    const query =
      "UPDATE Reservation SET Status = '"+
      reservation.status +
      `' WHERE ReservationId = ${reservationId} AND ApartmentId = ${apartmentId} AND UserId = ${req.userId}`

      if (reservation.status !== 'ACCEPTED' && reservation.status !== 'INITIAL' && reservation.status !== 'REJECTED') {
        const msg1 = 'Your status input is invalid. Please use REJECTED, ACCEPTED or Initial'
        logger.trace(msg1)
        const errorObject = {
          message: msg1,
          code: 401
        }
        next(errorObject)
      } else {
        logger.debug(query)
        database.executeQuery(query, (err, rows) => {
          // verwerk error of result
          if (err) {
            const errorObject = {
              message: 'Er ging iets mis in de database.',
              code: 500
            }
            next(errorObject)
          }
          if (rows) {
             if (rows.rowsAffected[0] === 0) {
              const msg = 'You do not have sufficient permissions, or the Reservation does not exist.'
              logger.trace(msg)
              const errorObject = {
                message: msg,
                code: 401
              }
              next(errorObject)
            } else {
              res.status(200).json({ result: 'Succes!' })
            }
          }
        })
      }
     },
     
  deleteReservationById: function(req, res, next) {
    logger.info('deleteReservationByiId aangeroepen')
    const reservationId = req.params.reservationId
    const apartmentId = req.params.apartmentId
    const userId = req.userId

    const query = `DELETE FROM Reservation WHERE ApartmentId=${apartmentId} AND UserId=${userId} AND ReservationId = ${reservationId}`
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        logger.trace('Could not delete apartment: ', err)
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        if (rows.rowsAffected[0] === 0) {
          // query ging goed, maar geen apartment met de gegeven ApartmentId EN userId.
          // -> retourneer een error!
          const msg = 'Apartment not found or you have no access to this apartment!'
          logger.trace(msg)
          const errorObject = {
            message: msg,
            code: 401
          }
          next(errorObject)
        } else {
          res.status(200).json({ result: 'Succes!' })
        }
      }
    })
  }
}
