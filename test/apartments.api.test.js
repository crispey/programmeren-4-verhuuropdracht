const chai = require('chai')
const chaiHttp = require('chai-http')
const jwt = require('jsonwebtoken')
const server = require('../app')

chai.should()
chai.use(chaiHttp)

const endpointapartments = '/api/apartments'
const endpointreservaton = '/api/apartments/1232/reservations'
const authorizationHeader = 'Authorization'
let token

//
// Deze functie wordt voorafgaand aan alle tests 1 x uitgevoerd.
//
before(() => {
  console.log('- before: get valid token')
  // We hebben een token nodig om een ingelogde gebruiker te simuleren.
  // Hier kiezen we ervoor om een token voor UserId 1 te gebruiken.
  const payload = {
    UserId: 1
  }
  jwt.sign({ data: payload }, 'secretkey', { expiresIn: 2 * 60 }, (err, result) => {
    if (result) {
      token = result
    }
  })
})

describe('Apartments API POST', () => {
  it('should return a valid id when posting a valid object', done => {
    chai
      .request(server)
      .post(endpointapartments)
      .set(authorizationHeader, 'Bearer ' + token)
      .send({
        description: 'description',
        streetaddress: 'streetaddress',
        postalcode: '3388AB',
        city: 'testing'
      })
      .end((err, res) => {
        res.should.have.status(201)
        res.body.should.be.a('object')

        const result = res.body.result
        const apartment = result[0]

        apartment.should.have.property('StreetAddress')
        apartment.should.have.property('Description')
        apartment.should.have.property('PostalCode')
        apartment.should.not.have.property('Password')
        done()
      })
  })
})

describe('Apartments API POST NO HEADER', () => {
    it('Should return an error. ', done => {
      chai
        .request(server)
        .post(endpointapartments)
        .set(authorizationHeader, 'Bearer ')
        .send({
          description: 'description',
          streetaddress: 'streetaddress',
          postalcode: '3388AB',
          city: 'testing'
        })
        .end((err, res) => {
          res.should.have.status(401)
          done()
        })
    })
  })
  
describe('Apartment API GET', () => {
  it('should return an array of Apartments', done => {
    chai
      .request(server)
      .get(endpointapartments)
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('object')

      const result = res.body.result
      result.should.be.an('array')
      const apartment = result[0]

      apartment.should.have.property('StreetAddress')
      apartment.should.have.property('Description')
      apartment.should.have.property('PostalCode')
      apartment.should.not.have.property('Password')
      done()
     })
    })
})
describe('Apartment API GET', () => {
  it('should return an array of Apartments', done => {
    chai
      .request(server)
      .get(endpointapartments)
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('object')

      const result = res.body.result
      result.should.be.an('array')
      const apartment = result[0]

      apartment.should.have.property('StreetAddress')
      apartment.should.have.property('Description')
      apartment.should.have.property('PostalCode')
      done()
     })
    })
})
describe('Reservation API POST', () => {
  it('should return a correct code', done => {
    chai
      .request(server)
      .post(endpointreservaton)
      .set(authorizationHeader, 'Bearer ' + token)
      .send({
        status: 'Initial',
        startdate: '2010-2-2',
        enddate: '2012-2-1'
      })
    .end((err, res) => {
      res.should.have.status(201)
      res.body.should.be.a('object')

      const result = res.body.result
      result.should.be.an('array')
      done()
     })
    })
})
describe('Reservations API GET', () => {
  it('should return an array of Reservations', done => {
    chai
      .request(server)
      .get(endpointreservaton)
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('object')

      const result = res.body.result
      result.should.be.an('array')
      const apartment = result[0]

      apartment.should.have.property('ReservationId')
      apartment.should.have.property('ApartmentId')
      apartment.should.have.property('StartDate')
      apartment.should.have.property('EndDate')
      apartment.should.have.property('UserId')
      apartment.should.have.property('Status')
      
      done()
     })
    })
  })